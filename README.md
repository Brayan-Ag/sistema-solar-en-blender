# Sistema Solar en Blender

Primer proyecto en un software libre utilizando la herramienta Blender para la creación de una animación de un pequeño sistema solar simplificado.

## Objetivos

General: Construir una simplificación del sistema solar en Blender.

Específicos:
- Acercar a las personas al tema de la astronomía.
- Contribuir al aprendizaje de los niños con una animación didáctica.

## Justificación

Ampliar mis conocimientos con respecto a herramientas de uso libre que puedan llegar a facilitar algunas áreas del campo científico como lo es la astronomía.

## Alcance y Limitaciones

Alcance: Representación a escala del sistema solar de forma simple.

Limitaciones: Conocimientos previos sobre física y el uso de Blender para hacer animaciones.

## Beneficiarios

Estudiantes, profesores y entusiastas de la astronomía, quienes quieran entender un poco el funcionamiento del sistema solar.

## Clonación del Proyecto

lo primero será tener instalado Blender previamente en tu equipo, para distribuciones basadas en Arch podemos instalarlo usando:


```bash
sudo pacman -S blender
```
o usando un AUR helper:

```bash
yay -S blender
```

Lo siguiente será clonar el proyecto usando:

```bash
git clone https://gitlab.com/Brayan-Ag/sistema-solar-en-blender.git
```
Ahora solo te queda abrir el proyecto en Blender

¡Ya puedes disfrutar de mi pequeña animación!

## Licencia

[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)
